[[tags: manual]]
[[toc:]]

== Module scheme

This module is intended for backwards compatibility with older CHICKEN code
and exports a subset of the R7RS procedures in {{(scheme base)}}, {{(scheme char)}},
 {{(scheme complex)}}, {{(scheme cxr)}}, {{(scheme eval)}}, {{(scheme file)}},
{{(scheme inexact)}}, {{(scheme lazy)}}, {{(scheme load)}},
{{(scheme read)}}, {{(scheme repl)}}, {{(scheme write)}} and all macros that correspond to
the bindings available by default in R5RS.

The bindings of this module are available by default at toplevel in interpreted
and compiled code.

For portable code is recommended to put toplevel code into modules and
import {{(scheme base)}} which provides the base R7RS environment.

---
Previous: [[Included modules]]

Next: [[Module (scheme r4rs)]]
